import math


class Point2D:
    def __init__(self, x: float, y: float) -> None:
        self.__x = x
        self.__y = y
        pass

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def distance(self, line=None, point=None) -> float:
        res = 0.0
        if line:
            res = self.__distance_to_line(line)
        elif point:
            res = self.__distance_to_point(point)

        return res

    def __distance_to_line(line):
        """
        Da implementare
        """
        pass

    def __distance_to_point(self, point) -> float:
        """
        Calcola la distanza tra sè stesso e un altro punto
        Prende in input un oggetto Point
        Sarebbe carino se prendesse anche le tuple
        """
        res = 0.0
        x1 = point.x
        y1 = point.y

        res = math.sqrt((x1-self.x)**2 + (y1-self.y)**2)

        return res

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        """
        Quando fai print() su un oggetto point stampa una tupla
        con le coordinate
        """
        res = str((self.x, self.y))
        return res


class Line2D:
    def __init__(self, A: Point2D, B: Point2D) -> None:
        self.__A = A
        self.__B = B
        self.__m = self.__calculate_m()
        self.__q = self.__calculate_q()

    @property
    def A(self):
        return self.__A

    @property
    def B(self):
        return self.__B

    @property
    def m(self):
        return self.__m

    @property
    def q(self):
        return self.__q

    def __calculate_m(self) -> float:
        res = 0.0
        try:
            res = (self.A.y-self.B.y)/(self.A.x-self.B.x)
        except:
            pass

        return res

    def __calculate_q(self) -> float:
        res = 0.0
        try:
            res = ((self.A.x*self.B.y)-(self.B.x*self.A.y)) / \
                (self.A.x-self.B.x)
        except:
            pass
        return res

    def intersect(self, A: Point2D, B: Point2D) -> Point2D:
        """
        Calcola il punto di intersezioni con un segmento di vertici [A,B]
        Avendo creato l'oggetto Segment sarebbe carino implementare la possibilità
        di passare il segment come parametro.

        Il metodo è semplice:
        - creo la retta giacente del segmento
        - se non sono parallele (controllo terribile con un try/except -> da migliorare)
            calcola il punto di intersezione
        - per la mitica unica ed inimitabile disuguaglianza triagolare
            il punto C appena trovato appartiene al segmento AB se e solo se
            AC+AB<=AB

        Il sistema è risoldo con Cramer quindi se il segmento è verticale
        ti attacchi al cazzo. Devi sistemare sto bug
        """
        res = None
        retta_giacente = Line2D(A=A, B=B)
        a1 = -self.m
        b1 = 1
        c1 = self.q
        a2 = -retta_giacente.m
        b2 = 1
        c2 = retta_giacente.q

        det = (a1*b2)-(a2*b1)
        Dx = (c1*b2)-(c2*b1)
        Dy = (a1*c2)-(a2*c1)

        try:
            tmp = Point2D(Dx/det, Dy/det)
            distance_A = tmp.distance(point=A)
            distance_B = tmp.distance(point=B)
            length = A.distance(point=B)
            if distance_A+distance_B <= length:
                res = tmp
        except:
            pass

        return res


class Segment2D(Line2D):
    """
    Al momento è un'implementazione di merda, ma un segmento nel mio mondo
    non è altro che una retta definita a partire sempre da 2 punti
    ma con l'aggiunta di avere una lunghezza.
    Eredita tutti i metodi della retta ed è per questo che è sbagliato
    -> il metodo intersect non funziona se applicato da un segmento su un altro
    """
    @property
    def length(self):
        return self.A.distance(self.B)


class Polygon2D:
    def __init__(self, points, polygonVertices) -> None:
        """
        Prendo quella merda di input e lo trasformo nelle mie classi
        che hanno dei metodi con un cazzo di senso per non diventare idioti
        """
        self.points = [Point2D(p[0], p[1]) for p in points]
        self.polygonVertices = polygonVertices
        self.sides = self.__build_segments()

    def __build_segments(self) -> list:
        """
        Sembra una porcata ma in realtà è furba. Prendo i punti di input
        e ci costruisco i segmenti in ordine. Visto che l'ordine è dato da
        polygonVertices ciclo su quei valori. Il modulo i % n_vertices serve per concatenare
        l'ultimo vertice al primo senza dover fare un altro if.
        """
        res = []
        n_vertices = len(self.polygonVertices)
        for i in range(n_vertices):
            index_A = self.polygonVertices[i % n_vertices]
            index_B = self.polygonVertices[(i+1) % n_vertices]
            point_A = self.points[index_A]
            point_B = self.points[index_B]
            segment_tmp = Segment2D(point_A, point_B)
            res.append(segment_tmp)

        return res

    def cut(self, line: Line2D) -> list:
        """
        Con tutte le classi ed helper functions di cui sopra il cut è molto semplice:
        Ciclo su tutti i lati del poligono e controllo con la funzione intersect se la linea
        intercetta un lato. L'output è da migliorare e manca svariato error handling,
        ma credo sia un buon punto di partenza
        """
        res = []
        for side in self.sides:
            intersection = line.intersect(side.A, side.B)
            res.append(intersection)
        return res
