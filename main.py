from Plane2D import *


if __name__ == "__main__":
    quadrato_points = [(0, 0), (1, 0), (1, 1), (0, 1)]
    quadrato_polygonVertices = [0, 1, 2, 3]

    r1 = Line2D(Point2D(0, -0.2), Point2D(2, 0.4))

    quadrato = Polygon2D(quadrato_points, quadrato_polygonVertices)
    print(quadrato.cut(r1))
